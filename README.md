This currently contains the initial check-in of proof-of-concept code. 
Need to split out each puppet module into its own repo. This aalso 
uses Vagrant for testing/deployment with the included bootstrap script.
Much of the bootstrap can be more generalized and cleaned up with, say
an OS package repository. On the uupside, this code is probably >90%
platform agnostic.

# How to execute on EL7 hosts (tested on CentOS 7)
1. Edit "bootstrap.sh" and set the HOST, SITE, and DOMAIN vars. 
(This can be further automated on deployment). 

2. Execute the script from an account that doesn't require a password with
sudo (it's likely that it will execute within a typical 15-minute sudo window)

3. Edit the dropped "rock.yaml" file in the user home dir and copy to the path
indicated in the comment.

4. Run "sudo puppet apply /etc/puppet/manifests/site.pp" (NOTE: This assumes the
non-enterprise version of puppet. The paths are different, I think).

# Run with an existing vagrant setup.
1. Run "vagrant up"
2. Complete steps 3 & 4 above.

