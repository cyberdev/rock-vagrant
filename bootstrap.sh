#!/usr/bin/env bash

# TODO: Figure out how to get this from cloud-init or similar
export HOST="sensor001"
export SITE="cyberdev"
export DOMAIN="cyberdev.lan"

# This gets detected
major_version=
os=
host=


unknown_os ()
{
  echo "Unfortunately, your operating system distribution and version are not supported by this script."
  echo "Please email support@criticalstack.com and we will be happy to help."
  exit 1
}

if [ -e /etc/os-release ]; then
  . /etc/os-release
  major_version=`echo ${VERSION_ID} | awk -F '.' '{ print $1 }'`
  os=${ID}

elif [ `which lsb_release 2>/dev/null` ]; then
  # get major version (e.g. '5' or '6')
  major_version=`lsb_release -r | cut -f2 | awk -F '.' '{ print $1 }'`

  # get os (e.g. 'centos', 'redhatenterpriseserver', etc)
  os=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`

elif [ -e /etc/oracle-release ]; then
  major_version=`cut -f5 --delimiter=' ' /etc/oracle-release | awk -F '.' '{ print $1 }'`
  os='ol'

elif [ -e /etc/fedora-release ]; then
  major_version=`cut -f3 --delimiter=' ' /etc/fedora-release`
  os='fedora'

elif [ -e /etc/redhat-release ]; then
  os_hint=`cat /etc/redhat-release  | awk '{ print tolower($1) }'`
  if [ "${os_hint}" = "centos" ]; then
    major_version=`cat /etc/redhat-release | awk '{ print $3 }' | awk -F '.' '{ print $1 }'`
    os='centos'
  elif [ "${os_hint}" = "scientific" ]; then
    major_version=`cat /etc/redhat-release | awk '{ print $4 }' | awk -F '.' '{ print $1 }'`
    os='scientific'
  else
    major_version=`cat /etc/redhat-release  | awk '{ print tolower($7) }' | cut -f1 --delimiter='.'`
    os='redhatenterpriseserver'
  fi

else
  aws=`grep Amazon /etc/issue 2>&1 >/dev/null`
  if [ "$?" = "0" ]; then
    major_version='6'
    os='aws'
  else
    unknown_os
  fi
fi

if [[ ( -z "${os}" ) || ( -z "${major_version}" ) || ( "${os}" = "opensuse" ) ]]; then
  unknown_os
fi

#################
# Set the hostname
sudo hostnamectl set-hostname "${HOST}.${DOMAIN}"


#################
if [ "${os}" == "rhel" ]
  then
  # If RHEL, enable the optionals repo
  # TODO: This requires RHEL provisioning first
  export REPO=$(sudo subscription-manager repos --list | grep optional-rpms | awk -F: '{ gsub(/^[ \t]+/, "", $2); print $2 }')
  sudo subscription-manager repos --enable "${REPO}"
fi

# Install puppet repo
#case "${major_version}" in
#  "7")  echo "Setting up EL 7"
#      sudo yum -y localinstall http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
#      ;;
#
#  "6")  echo "Setting up EL 6"
#      sudo yum -y localinstall http://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm
#      ;;
#
#  *)  echo "This script doesn't support your release yet" 1>&2
#	  exit 1
#      ;;
#esac

# Install cyberdev repo for bro packages and such
# TODO: Ditch this script in favor of a repo-release package
curl https://packagecloud.io/install/repositories/dcode/cyberdev/script.rpm.sh | sudo bash

# TODO: consider ditching the puppetlabs repo and just install epel-release directly
#sudo yum -y update
#sudo yum -y install puppet 
#
## Quick and easy EPEL installation
#sudo puppet module install stahnma-epel
#sudo puppet apply -e 'include epel'
#
## Remove unneccessary items here
#sudo yum -y remove firewalld

sudo yum -y install https://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
# Install all dependent packages
# TODO: I'd like to limit package installs to a single instace, with an update
sudo yum -y shell << EOF
remove firewalld
update
install puppet augeas iptables-services dkms pfring libpcap-pfring bro brocontrol \
  gperftools-libs git puppet-rock java-1.8.0-oracle-headless kafka suricata nginx-spegno
run
EOF

####### Clear out the cruft from firewalld
sudo service iptables stop

sudo tee /etc/puppet/hiera.yaml <<EOF
---
:backends:
  - yaml
:yaml:
  :datadir: /etc/puppet/hieradata
:hierarchy:
  - "node/%{::fqdn}"
  - common

EOF


# Set base YAML config
sudo mkdir -p /etc/puppet/hieradata/node
sudo mkdir -p /etc/puppet/manifests
sudo tee -a /etc/puppet/manifests/site.pp <<EOF
hiera_include('classes')
EOF

sudo tee /etc/puppet/hieradata/node/$(facter fqdn).yaml <<EOF
---
classes:
  - roles::rocksensor

profiles::site::site_name: "${SITE}"
profiles::sensor::sensor_name: "${HOST}"

# Allow any remote access by default
profiles::sensor::trusted_net: "0.0.0.0/0"
profiles::sensor::monitor_if: "enp0s3"
profiles::sensor::monitor_nets:
  - 10.0.0.0/8      # RFC 1918
  - 100.64.0.0/10
  - 172.16.0.0/12   # RFC 1918
  - 192.0.2.0/24    # TEST-NET-1
  - 192.168.0.0/16  # RFC 1918
  - 198.51.100.0/24 # TEST-NET-2
  - 203.0.113.0/24  # TEST-NET-3
  - 2001:db8::/32   # TEST-NETv6

profiles::sensor::pfring_cpus: 
  - 1
  - 4
profiles::sensor::pfring_ringsize: 32768

nginx::package_name: nginx-spnego
nginx::manage_repo: false
EOF

sudo puppet apply /etc/puppet/manifests/site.pp


#######  Logstash

# Install logstash module
sudo puppet module install elasticsearch-logstash
# Install elasticsearch modul
sudo puppet module install elasticsearch-elasticsearch

tee elastic-logstash.pp << EOF
class { 'logstash': }
class { 'elasticsearch':   ensure => 'present' }

EOF
sudo puppet apply elastic-logstash.pp

## Force bro to write JSON logs
sudo tee /opt/bro/share/bro/site/scripts/json-logs.bro << EOF
@load tuning/json-logs

redef LogAscii::json_timestamps = JSON::TS_ISO8601;
redef LogAscii::use_json = T;
EOF

sudo tee -a /opt/bro/share/bro/site/local.bro << EOF
@load scripts/json-logs
EOF

sudo /opt/bro/bin/broctl install
sudo /opt/bro/bin/broctl restart

## Configure logstash to read bro logs
sudo tee /etc/logstash/conf.d/logstash-bro.conf << EOF
input {
  file {
    path => '/var/opt/bro/logs/current/*.log'
    exclude => [
                '/var/opt/bro/logs/current/stderr.log',
                '/var/opt/bro/logs/current/stdout.log',
                '/var/opt/bro/logs/current/communication.log',
                '/var/opt/bro/logs/current/loaded_scripts.log'
                ]
    codec => "json"
    start_position => "beginning"
    #sincedb_path => "/dev/null"
    type => "bro"
    add_field => { "[@metadata][stage]" => "bro_raw" }
  }
}

filter {
  if [@metadata][stage] == "bro_raw" {
    date {
      match => ["ts", "ISO8601"]
    }

    ruby {
      code => "event['path'] = event['path'].split('/')[-1].split('.')[0]"
    }

  }
}

output {
 if [@metadata][stage] == "bro_raw" {
    kafka { topic_id => "bro_raw" }
  }
}
EOF

## Configure logstash to forward bro data from kafka
sudo tee /etc/logstash/conf.d/kafka-bro.conf << EOF
input {
  kafka {
    topic_id => "bro_raw"
    add_field => { "[@metadata][stage]" => "bro_kafka" }
  }
}

output {
  if [@metadata][stage] == "bro_kafka" {
    #stdout { codec => rubydebug }
    elasticsearch { }
  }
}
EOF

## Install Kibana 4
# TODO: Package this
curl -L -O -J https://download.elastic.co/kibana/kibana/kibana-4.0.2-linux-x64.tar.gz
cd /opt; sudo mkdir kibana; cd kibana
sudo tar -xzv --strip-components=1 -f /home/vagrant/kibana-4.0.2-linux-x64.tar.gz


## Kibana 4 Startup
sudo tee /etc/systemd/system/kibana.service << EOF
[Unit]
Description=Kibana 4 Web Interface
Requires=elasticsearch-rock.service
After=elasticsearch-rock.service
After=logstash.service

[Service]
ExecStart=/opt/kibana/bin/kibana
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=kibana
User=root
Group=root
Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
EOF

sudo chkconfig kibana on
sudo service kibana start

# FIXME: Disable iptables for now
sudo service iptables stop

## Create zookeeper user and group
sudo useradd --system -s /bin/false -b /opt/kafka/ --no-create-home zookeeper

## Zookeeper startup
sudo tee /etc/systemd/system/zookeeper.service << EOF
[Unit]
Description=Zookeeper distributed coordination server
After=network.target

[Service]
User=zookeeper
Group=zookeeper
SyslogIdentifier=zookeeper
Environment="KAFKA_LOG4J_OPTS=-Dlog4j.configuration=file:/opt/kafka/config/log4j.properties"
Environment="KAFKA_HEAP_OPTS=-Xmx512M -Xms512M"
Environment="ZOOKEEPER_ARGS=-name zookeeper -loggc"
Environment="ZOOKEEPER_PROPS=/opt/kafka/config/zookeeper.properties"
EnvironmentFile=-/etc/opt/sensor/zookeeper
ExecStart=/opt/kafka/bin/kafka-run-class.sh  org.apache.zookeeper.server.quorum.QuorumPeerMain \${ZOOKEEPER_PROPS}
#ExecStart=/usr/bin/java   -Dzookeeper.log.dir=/var/log/zookeeper   -Dzookeeper.root.logger=INFO,ROLLINGFILE   -cp /usr/share/java/zookeeper/*   -Dlog4j.configuration=file:/etc/zookeeper/log4j.properties   -Dcom.sun.management.jmxremote   -Dcom.sun.management.jmxremote.local.only=false   org.apache.zookeeper.server.quorum.QuorumPeerMain   /etc/zookeeper/zoo.cfg

[Install]
WantedBy=multi-user.target
EOF

# Enable zookeeper on boot
sudo chkconfig zookeeper on
sudo service zookeeper start

## Create kafka user and group
sudo useradd --system -s /bin/false -b /opt/kafka/ --no-create-home kafka

## Add zookeeper to kafka group and create logs dir
sudo usermod -aG kafka zookeeper
sudo mkdir -p /opt/kafka/logs
sudo chgrp kafka /opt/kafka/logs
sudo chmod 775 /opt/kafka/logs

## Create Kafka service file
sudo tee /etc/systemd/system/kafka.service << EOF
[Unit]
Description=Kafka distributed logging server
Requires=zookeeper.service
After=zookeeper.service

[Service]
User=kafka
Group=kafka
SyslogIdentifier=kafka
Environment="KAFKA_LOG4J_OPTS=-Dlog4j.configuration=file:/opt/kafka/config/log4j.properties"
Environment="KAFKA_HEAP_OPTS=-Xmx1G -Xms1G"
Environment="KAFKA_ARGS=-name kafkaServer -loggc"
Environment="KAFKA_PROPS=/opt/kafka/config/server.properties"
EnvironmentFile=-/etc/opt/sensor/kafka
ExecStart=/opt/kafka/bin/kafka-run-class.sh  kafka.Kafka \${KAFKA_PROPS}
#ExecStart=/usr/bin/java   -Dkafka.log.dir=/var/log/kafka   -Dkafka.root.logger=INFO,ROLLINGFILE   -cp /usr/share/java/kafka/*   -Dlog4j.configuration=file:/etc/kafka/log4j.properties   -Dcom.sun.management.jmxremote   -Dcom.sun.management.jmxremote.local.only=false   org.apache.kafka.server.quorum.QuorumPeerMain   /etc/kafka/zoo.cfg

[Install]
WantedBy=multi-user.target
EOF

## Enable kafka on boot
sudo chkconfig kafka on
sudo service kafka start 

# Let kafka settle
sleep 3


## Create the bro_raw topic
sudo /opt/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic bro_raw

# Restart logstash to sync with kafka
sudo service logstash restart

