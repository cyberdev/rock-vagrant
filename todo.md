[ ] Configure passive interface for the monitor if. Currently uses DHCP.
[ ] Install Logstash and Elasticsearch via RPM
[ ] Install puppet module for logstash
[ ] Populate sensor-scripts package configuration with puppet
[ ] Populate netsniff-ng puppet module
[ ] Package kibana
[ ] Figure out how to get nginx to auth with FreeIPA (feasible?)
[ ] Add ${BRO\_DIR}/bin to path
[ ] Create zookeeper service user and group accounts
[ ] Create kafka service user and group accounts
[ ] Troubleshoot error in logstash. Happens repeatedly
  A plugin had an unrecoverable error. Will restart this plugin.
  Plugin: <LogStash::Inputs::File path=>["/var/opt/bro/logs/current/\*.log"], exclude=>["/var/opt/bro/logs/current/stderr.log", "/var/opt/bro/logs/current/stdout.log", "/var/opt/bro/logs/current/communication.log", "/var/opt/bro/logs/current/loaded\_scripts.log"], start\_position=>"beginning", type=>"bro", add\_field=>{"[@metadata][stage]"=>"bro\_raw"}, delimiter=>"\n">
  Error: undefined method `[]' for 1432147315.439476:Float {:level=>:error}
